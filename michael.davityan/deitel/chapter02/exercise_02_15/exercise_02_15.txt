question of the exercise!!!: State the order of evaluation of the operators in each of the following C++ statements and show the value of x after each 
statement is performed.
a. x = 7 + 3 * 6 / 2 - 1 ;
b. x = 2 % 2 + 2 * 2 - 2 / 2 ;
c. x = ( 3 * 9 * ( 3 + ( 9 * 3 / ( 3 ) ) ) );

answer!!!.
1 - first
2 - second
3 - third
4 - fourth
5 - fifth
6 - sixth
7 - seventh

a. x = 7 + 3 * 6 / 2 - 1 ;  x = 15 
     5   3   1   2   4

b. x = 2 % 2 + 2 * 2 - 2 / 2;  x = 3 
     6   1   4   2   5   3

c. x = ( 3 * 9 * ( 3 + ( 9 * 3 / ( 3 ) ) ) ); x = 324
     7     5   6     4     2   3   1
