#include <iostream>

int
main()
{
    std::cout << "(a)\n";
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    std::cout << "(b)\n";
    for (int row = 10; row >= 1; --row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    std::cout << "(c)\n";
    int count = 1;
    for (int row = 10; row >= 1; --row) {
        for (int column = 2; column <= row; ++column) {
            std::cout << " ";                    
        }
        for (int column = 1; column <= count; ++column) {
            std::cout << "*";                    
        }
        ++count;
        std::cout << std::endl;
    }

    std::cout << "(d)\n";
    count = 10;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 2; column <= row; ++column) {
            std::cout << " ";                    
        }
        for (int column = count; column >= 1; --column) {
            std::cout << "*";                    
        }
        --count;
        std::cout << std::endl;
    }

    return 0;
}
