#include <iostream>

int
main()
{
    int countOfNumbers;
    std::cin >> countOfNumbers;

    int sumOfAll = 0;
    for (int i = 1; i <= countOfNumbers; ++i) {
        int eachNumber;
        std::cin >> eachNumber;
        sumOfAll += eachNumber;
    }

    std::cout << sumOfAll << std::endl;
    return 0;
}
