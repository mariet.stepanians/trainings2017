#include "Employee.hpp"
#include <iostream>
#include <string>

Employee::Employee(std::string name, std::string surname, int salary)
{
    setName(name);
    setSurname(surname);
    setSalary(salary);
}

void 
Employee::setName(std::string name) 
{
    name_ = name;
}

void 
Employee::setSurname(std::string surname)
{
    surname_ = surname;
}

void 
Employee::setSalary(int salary)
{
    if (salary < 0) {	    
        salary_ = 0;
	return;
    }
    salary_ = salary;
}

std::string 
Employee::getName()
{
    return name_;
}

std::string 
Employee::getSurname()
{
    return surname_;
}

int 
Employee::getSalary()
{
    return salary_;
}

