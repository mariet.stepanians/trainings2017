#include <iostream>

int
main()
{
    double radius;

    std::cout << "Enter the radius of circle: ";
    std::cin >> radius;

    if (radius < 1) {
        std::cerr << "Error 1: Radius can't be Negative or zero." << std::endl;
        return 1;
    }

    double perimeter = 2 * 3.14159 * radius;
    double area = 3.14159 * radius * radius;
    double diameter = 2 * radius;

    std::cout << "The diameter of circle is " << diameter << std::endl;
    std::cout << "The perimeter of circle is " << perimeter << std::endl;
    std::cout << "The area of circle is " << area << std::endl;

    return 0;
}
