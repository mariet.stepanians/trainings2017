#include <iostream>

int
main ()
{
    int number1, number2;

    std::cout << "Enter two numbers: ";
    std::cin >> number1 >> number2;

    int sum = number1 + number2;
    std::cout << "Sum is: " << sum << std::endl;

    int product = number1 * number2;
    std::cout << "Product is: " << product << std::endl;

    int difference = number1 - number2;
    std::cout << "Difference is: " << difference << std::endl;

    if (0 == number2) {
        std::cout << "Error 1: quotient couldn't be calculated, the second number should not be zero." << std::endl;
        return 1;
    }

    int quotient = number1 / number2;
    std::cout << "Quotient is: " << quotient << std::endl;

    return 0;
}

