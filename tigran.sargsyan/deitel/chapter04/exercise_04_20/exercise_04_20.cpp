#include "Analysis.hpp"

#include <iostream>

int
main()
{
    Analysis application;
    application.processExamResults();
    return 0;
}

