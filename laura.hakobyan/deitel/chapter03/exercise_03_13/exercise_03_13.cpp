#include "Invoice.hpp"
#include <iostream>

int
main()
{
    Invoice invoice1("No. 0054", "Chocolate", 150, 40);
    std::cout << "Part number is: " << invoice1.getPartNumber() << std::endl;
    std::cout << "Part description: " << invoice1.getPartDescription() << std::endl;
    std::cout << "Quantity of the item being purchased is: " << invoice1.getPurchaseQuantity() << std::endl;
    std::cout << "Price per item is: " << invoice1.getPricePerItem() << " AMD" << std::endl;
    std::cout << "Invoice amount of purchases is: " << invoice1.getInvoiceAmount() << " AMD" << std::endl;
    return 0;
} 

