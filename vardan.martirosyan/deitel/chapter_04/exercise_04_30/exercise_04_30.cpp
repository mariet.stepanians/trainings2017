#include <iostream>

int
main()
{
    double radius;
    std::cout << "Enter radius: ";
    std::cin  >> radius;
    
    if (radius < 0) {
        std::cerr << "Error 1: Radius can not be negative." << std::endl;
        return 1;
    }
    double diameter = radius * 2;
    std::cout << "Diameter is " << diameter << std::endl;

    double length = 2 * 3.14159 * radius;
    std::cout << "Length is " << length << std::endl;

    double area = length * radius;
    std::cout << "Area is " << area << std::endl;

    return 0;
}
