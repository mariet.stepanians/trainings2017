#include <iostream>

int
main()
{
    int binary;
    std::cout << "Enter binary number: ";
    std::cin  >> binary;
        
    if (binary < 0) {
        std::cerr << "Error 1: Binary number can not be negative." << std::endl;
        return 1;
    }
    
    int binaryBase = 1, decimal = 0;
    while (binary > 0) {   
        int binaryDigit = binary % 10;
        if (binaryDigit > 1) {
            std::cerr << "Error 2: This in not a binary number." << std::endl;
            return 2;
        }
        binary /= 10;
        decimal += (binaryDigit * binaryBase);
        binaryBase *= 2;
    }

    std::cout << "Decimal number is " << decimal << std::endl;

    return 0;
}

