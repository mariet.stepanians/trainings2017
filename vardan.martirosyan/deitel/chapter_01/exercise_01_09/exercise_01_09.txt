a)Because the internal organization of object-oriented programming, texts are usually built with the application of techniques of structured programming.

b)Object-oriented design uses classification tools when objects of a certain class, for instance, a class of vehicles, have the same characteristics.
Object-oriented design uses inheritance relationships when new classes of objects are produced by absorbing the characteristics of existing classes and adding their own unique characteristics to them. Object-oriented designs also  model the communication between objects.

c)People send each other communicational messages.

d)Attributes and actions of object-oriented design encapsulates in objects - the attributes and actions of the object are closely connected to each other.
