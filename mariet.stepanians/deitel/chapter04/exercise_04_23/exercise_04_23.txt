a. 
    x = 9;
    y = 11;
    output ->  *****
               $$$$$

    x = 11;
    y = 9;
    output ->  $$$$$

b.
    x = 9;
    y = 11;
    output ->  *****

    x = 11;
    y = 9;
    output ->  #####
               $$$$$
